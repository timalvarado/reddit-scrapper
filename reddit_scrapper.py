import argparse
import smtplib
import requests
import praw

def main():
    parser = argparse.ArgumentParser(
        description='Emails top posts of the day for a given subreddit'
    )
    parser.add_argument(
        '--id',
        required=True,
        type=str,
        help='Client ID'
    )
    parser.add_argument(
        '--secret',
        required=True,
        type=str,
        help='Client Secret'
    )
    parser.add_argument(
        '-s',
        '--subreddit',
        required=False,
        type=str,
        help='Set the subreddit you would like to get the top posts from',
        default='all'
    )
    parser.add_argument(
        '--limit',
        required=False,
        type=int,
        help='Sets limit for amount of posts to return. Default=10',
        default=10
    )
    parser.add_argument(
        '--email',
        required=True,
        type=str,
        help='Email to send the results to'
    )

    args = parser.parse_args()
    reddit = praw.Reddit(client_id=args.id,
                         client_secret=args.secret,
                         user_agent='Top Posts on a subreddit for the day'
    )

    subreddit = reddit.subreddit(args.subreddit).top('day', limit=args.limit)
    posts = getPosts(subreddit)
    message = makeMessage(posts)
    sendEmail(message,args.email)

def getPosts(subreddits):  # Takes top 10 posts and adds it to a dictionary with title being key and url being value
    subreddit_submissions = {}
    for submission in subreddits:
        title = submission.title
        url = submission.shortlink
        subreddit_submissions[title] = url
    return subreddit_submissions

def makeMessage(dict_of_submissions):    # Creates message in email
    d = dict_of_submissions
    posts = list()
    for key, value in d.items():
        posts.append("{0}\n{1}".format(key,value))
    return posts

def sendEmail(posts, sendto):
    server = smtplib.SMTP('smtp.gmail.com', 587)
    server.starttls()
    server.login('username', 'password')
    subject = 'Reddit Scrapper'
    msg = '\n\n'.join(posts)
    msg = 'Subject: {}\n\n{}'.format(subject, msg)
    server.sendmail('from_addr', 'to_addr', msg)
    server.quit()

if __name__ == "__main__":
    main()
